# Crossover Project Evaluation Dockerfile
# 2017, Kenneth Jensen <kenneth@jensen.cf>

FROM maven
LABEL maintainer Kenneth Jensen

ENV SPRING_DATASOURCE_URL=jdbc:mysql://172.17.0.2:3306/journals?createDatabaseIfNotExist=true
ENV SPRING_DATASOURCE_USERNAME=root
ENV SPRING_DATASOURCE_PASSWORD=fwvXTv9gFNO4Lw

COPY . /usr/src/crossover-eval
WORKDIR /usr/src/crossover-eval
RUN mvn package

EXPOSE 8080

CMD ["java", "-jar", "./target/journals-1.0.jar"]
